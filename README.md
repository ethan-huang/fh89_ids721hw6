## Goals

- Add logging to a Rust Lambda function
- Integrate AWS X-Ray tracing
- Connect logs/traces to CloudWatch

## Steps

0. create a new user for this project
1. `cargo lambda new <Your Project Name> && cd <Your Project Name>`
2. change the main.rs file and Cargo.toml to add dependencies tracing = "0.1.40" and tracing-subscriber = "0.3.18"
3. Designed a rust function to input a string and then return a "double string" such as input is hello, then it returned "hellohello"
4. Run `cargo build --release` to build the application
5. Run `cargo deploy --region<region> --iam-role<your-role>'`to deploy the function
6. Added an API GATEWAY for the lambda function, and under the Configuration tab, enable X-ray active tracing and lambda insights monitoring.
7. Go to the test console to send a json object with key "data" with value "helloword"

## result

- ![API GATEWAY](/images/api.png)

- ![send the test string](/images/test.png)

- ![traces](/images/r1.png)

- ![segments timeline](/images/r2.png)
