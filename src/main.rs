use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};
use tracing::{info, warn, Level};
use tracing_subscriber::FmtSubscriber;

#[derive(Deserialize, Serialize)]
struct Input {
    data: String,
}

#[derive(Deserialize, Serialize)]
struct Output {
    result: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    // Initialze logging
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("Unable to global default");

    info!("Starting the redactr service...");

    let func = handler_fn(process_data);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn process_data(event: Input, _ctx: Context) -> Result<Output, Error> {
    // Reverse the input string
    let double = event.data.clone() + &event.data.clone();

    Ok(Output {
        result: double,
    })
}
